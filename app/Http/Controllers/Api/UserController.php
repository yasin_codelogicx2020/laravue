<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User as ResourcesUser;
use App\Profile;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $token = Str::random(80);
            Auth::user()->api_token = $token;
            Auth::user()->save();
            $admin = Auth::user()->isAdmin();
            return response()->json(['token' => $token, 'isAdmin' => $admin], 200);

        }

        return response()->json(['status' => 'Email Invalid or Password is Wrong'], 403);

    }

    public function index()
    {
        //
        // return response()->json(['users' => User::with('role')->get()], 200);
        return response()->json(['users' => ResourcesUser::collection(User::with('role')->get()),
            'roles' => Role::pluck('name')->all(),
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $role = Role::where('name', $request->role)->first();
        $user = new User([
            'id' => $request->id,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $user->role()->associate($role);
        // return response()->json(['user' => $user, 'role' => $role], 200);

        // //we are adding
        $user->save();
        $user->profile()->save(new Profile());
        return response()->json(['user' => $user], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newrole = Role::where('name', $request->role)->first();

        //

        $User = User::find($id);

        $User->role()->dissociate($User->role);
        $User->role()->associate($newrole);

        $User->name = $request->name;

        $User->save();
        return response()->json(['User' => $User], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id)->delete();
        Profile::where('user_id', $id)->delete();
        return response()->json(['User' => $User], 200);

        //
    }

    public function verify(Request $request)
    {
        return $request->user()->only('name', 'email');
    }

    public function verifyEmail(Request $request)
    {
        $request->validate(
            ['email' => 'required|unique:users',
            ]);

        return response()->json(['message' => 'Valid Email'], 200);
    }

    public function changePhoto(Request $request)
    {
        $user = User::find($request->user);
        $profile = Profile::where('user_id', $request->user)->first();
        $ext = $request->photo->extension();
        // $photo = $request->photo->storeAs('public/storage/images', Str::random(20) . "{$ext}", 'public
        // ');
        $photo = request('photo')->store('public/images');
        $photoname = $request->photo->hashName();

        $profile->photo = 'storage/images/' . $photoname;
        $user->profile()->save($profile);
        return response()->json(
            [
                'user' => $user,
                'profile' => $profile,
                'ext' => $ext,
                'photo' => $photo,
            ], 200);
    }

}
